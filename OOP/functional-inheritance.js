//1
function Machine(power) {
  this._enabled = false;

  var self = this;

  this.enable = function() {
    // используем внешнюю переменную вместо this
    self._enabled = true;
  };

  this.disable = function() {
    self._enabled = false;
  };

}

function CoffeeMachine(power) {
  Machine.apply(this, arguments);

  var waterAmount = 0;

  this.setWaterAmount = function(amount) {
    waterAmount = amount;
  };

  var parentEnable = this.enable;
  this.enable = function() {
      parentEnable(); // теперь можно вызывать как угодно, this не важен
      this.run();
    }

  function onReady() {
    alert( 'Кофе готово!' );
  }

  this.run = function() {
    if(!this._enabled) {
      throw new Error('Not enbaled!');
    }
    setTimeout(onReady, 1000);
  };

}

//2

function Machine(power) {
  this._enabled = false;

  var self = this;

  this.enable = function() {
    // используем внешнюю переменную вместо this
    self._enabled = true;
  };

  this.disable = function() {
    self._enabled = false;
  };

}

function CoffeeMachine(power) {
  Machine.apply(this, arguments);

  var waterAmount = 0;

  this.setWaterAmount = function(amount) {
    waterAmount = amount;
  };

  var parentEnable = this.enable;
  this.enable = function() {
    parentEnable(); // теперь можно вызывать как угодно, this не важен
    this.run();
  }
  
  var parentDisable = this.disable;
  this.disable =  function() {  
    parentDisable();
    clearTimeout(TimerId);
  }

  function onReady() {
    alert( 'Кофе готово!' );
  }

  this.run = function() {
    if(!this._enabled) {
      throw new Error('Not enbaled!');
    }
    var TimerId = setTimeout(onReady, 1000);
  };

}

//3


function Machine(power) {
  this._power = power;
  this._enabled = false;

  var self = this;

  this.enable = function() {
    // используем внешнюю переменную вместо this
    self._enabled = true;
  };

  this.disable = function() {
    self._enabled = false;
  };

}

function Fridge(power) {
  Machine.call(this, power);

  var food = [];

  this.addFood = function() {
    if(!this._enabled) {
      throw new Error("Turn it on first");
    }

    if(food.length + arguments.length > this._power/100) {
      throw new Error("Too much food. Can only have " + this._power/100);
    }
    var items = Array.from(arguments);
    console.log(items.length);
    food = food.concat(items);
  }

  this.getFood = function() {
    return food.slice();
  }
}

//4

function Machine(power) {
  this._power = power;
  this._enabled = false;

  var self = this;

  this.enable = function() {
    // используем внешнюю переменную вместо this
    self._enabled = true;
  };

  this.disable = function() {
    self._enabled = false;
  };

}

function Fridge(power) {
  Machine.call(this, power);

  var food = [];

  this.addFood = function() {
    if(!this._enabled) {
      throw new Error("Turn it on first");
    }

    if(food.length + arguments.length > this._power/100) {
      throw new Error("Too much food. Can only have " + this._power/100);
    }
    var items = Array.from(arguments);
    console.log(items.length);
    food = food.concat(items);
  }

  this.getFood = function() {
    return food.slice();
  }

  this.filterFood = function(func) {
    return food.filter(func);
  }

  this.removeFood = function(item) {
    var index = food.indexOf(item); 

    if(index > -1) {
      food.splice(index, 1);
    }
  }
}

//5

function Machine(power) {
  this._power = power;
  this._enabled = false;

  var self = this;

  this.enable = function() {
    // используем внешнюю переменную вместо this
    self._enabled = true;
  };

  this.disable = function() {
    self._enabled = false;
  };

}

function Fridge(power) {
  Machine.call(this, power);

  var food = [];

  this.addFood = function() {
    if(!this._enabled) {
      throw new Error("Turn it on first");
    }

    if(food.length + arguments.length > this._power/100) {
      throw new Error("Too much food. Can only have " + this._power/100);
    }
    var items = Array.from(arguments);
    console.log(items.length);
    food = food.concat(items);
  }

  this.getFood = function() {
    return food.slice();
  }

  this.filterFood = function(func) {
    return food.filter(func);
  }

  this.removeFood = function(item) {
    var index = food.indexOf(item); 

    if(index > -1) {
      food.splice(index, 1);
    }
  }

  var parentDisable = this.disable;

  this.disable = function() {
    if(food.length > 0) {
      throw new Error("Can't shut down. Still food in a fridge.");
    }

    parentDisable();
  }
}