//1

function User() {
    var firstName, surname;

    this.setFirstName = function (name) {
        if(name) firstName = name;            
    }

    this.setSurname = function(name) {
        if(name) surname = name;          
    }

    this.getFullName = function() {
        return firstName + " " + surname;
    }
}

//2

function CoffeeMachine(power, capacity) {
    //...
    this.setWaterAmount = function(amount) {
      if (amount < 0) {
        throw new Error("Значение должно быть положительным");
      }
      if (amount > capacity) {
        throw new Error("Нельзя залить воды больше, чем " + capacity);
      }
  
      waterAmount = amount;
    };
  
    this.getWaterAmount = function() {
      return waterAmount;
    };

    this.getPower = function() {
        return power;
    }
  
  }

// 3

function CoffeeMachine(power, capacity) {
    var waterAmount = 0;
  
    var WATER_HEAT_CAPACITY = 4200;
  
    function getTimeToBoil() {
      return waterAmount * WATER_HEAT_CAPACITY * 80 / power;
    }
  
    this.setWaterAmount = function(amount) {
      if (amount < 0) {
        throw new Error("Значение должно быть положительным");
      }
      if (amount > capacity) {
        throw new Error("Нельзя залить больше, чем " + capacity);
      }
  
      waterAmount = amount;
    };

    this.addWater = function(amount) {
        if (amount < 0) {
            throw new Error("Значение должно быть положительным");
          }
          if (amount + waterAmount > capacity) {
            throw new Error("Нельзя залить больше, чем " + capacity);
          }
      
          waterAmount += amount;
    }
  
    function onReady() {
      alert( 'Кофе готов!' );
    }
  
    this.run = function() {
      setTimeout(onReady, getTimeToBoil());
    };
  
  }  
  
// shoud be  this.addWater = function(amount) {
//     this.setWaterAmount(waterAmount + amount);
//   };


// 4

function CoffeeMachine(power, capacity) {
    var waterAmount = 0;
  
    var WATER_HEAT_CAPACITY = 4200;
  
    function getTimeToBoil() {
      return waterAmount * WATER_HEAT_CAPACITY * 80 / power;
    }
  
    this.setWaterAmount = function(amount) {
      // ... проверки пропущены для краткости
      waterAmount = amount;
    };
  
    this.getWaterAmount = function(amount) {
      return waterAmount;
    };
  
    var onReady = function() {
        alert( 'Кофе готов!' );
      }

    this.setOnReady = function(func) {
        onReady = func;
    }
  
    this.run = function() {
      setTimeout(onReady, getTimeToBoil());
    };
  
  }

  //5

  function CoffeeMachine(power, capacity) {
    var waterAmount = 0;
  
    var WATER_HEAT_CAPACITY = 4200;

    var running = false;
  
    function getTimeToBoil() {
      return waterAmount * WATER_HEAT_CAPACITY * 80 / power;
    }
  
    this.setWaterAmount = function(amount) {
      // ... проверки пропущены для краткости
      waterAmount = amount;
    };
  
    this.getWaterAmount = function(amount) {
      return waterAmount;
    };
  
    function onReady() {
      alert( 'Кофе готов!' );
    }
  
    this.setOnReady = function(newOnReady) {
      onReady = newOnReady;
    };

    this.isRunning = function() {
      return running;
    }
  
    this.run = function() {
      running = true;
      setTimeout(function() {
        running = false;
        onReady();
      }, getTimeToBoil());
    };
  
  }

