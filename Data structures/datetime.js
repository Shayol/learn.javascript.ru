// Создайте объект Date для даты: 20 февраля 2012 года, 3 часа 12 минут.

var date = new Date(2012, 1, 20, 15, 12);

// Создайте функцию getWeekDay(date), которая выводит текущий день недели в коротком формате „пн“, „вт“, … „вс“.

var today = new Date();

function getWeekDay(date) {
    var options = {
        weekday: "short"
    }
    return date.toLocaleString("ru", options);
}

// Напишите функцию, getLocalDay(date) которая возвращает день недели для даты date.

// День нужно возвратить в европейской нумерации, т.е. понедельник имеет номер 1, вторник номер 2, …, воскресенье – номер 7.

var today = new Date();

function getLocalDay(date) {
    return date.getDay() ? date.getDay() : 7;
}

// Создайте функцию getDateAgo(date, days), которая возвращает число, которое было days дней назад от даты date

var today = new Date();

function getDateAgo(date, daysAgo) {
    var newDate = new Date(date);
    newDate.setDate(newDate.getDate() - daysAgo);
    return newDate.getDate();
}

// Напишите функцию getLastDayOfMonth(year, month), которая возвращает последний день месяца.

function getLastDayOfMonth(year, month) {
    var nextMonth = new Date(year, month+1, 1);
    nextMonth.setDate(0);
    return nextMonth.getDate();
}

// Напишите функцию getSecondsToday() которая возвращает, сколько секунд прошло с начала сегодняшнего дня.

function getSecondsToday() {
    var today = new Date();
    var seconds = today.getHours() * 3600 + today.getMinutes()*60 + today.getSeconds();
    return seconds;
}

// Напишите функцию getSecondsToTomorrow() которая возвращает, сколько секунд осталось до завтра.

function getSecondsToTomorrow() {
    var today = new Date();
    var tommorow = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1);
    var seconds = (tommorow - today)/1000;
    return seconds;
}

// Напишите функцию formatDate(date), которая выводит дату date в формате дд.мм.гг

function formatDate(date) {
    var day = date.getDate() > 9 ? date.getDate() : "0" + date.getDate();
    var month = date.getMonth() + 1;
    var month = month < 10 ? "0" + month : month;
    var year = String(date.getFullYear()).slice(-2);
    return day + ":" + month + ":" + year;
}

// Напишите функцию formatDate(date), которая форматирует дату date

function formatDate(date) {
    var now = new Date();
    var diff = (now - date)/1000;

    if (diff < 1) return "just now";
    if (diff < 60) return Math.floor(diff) + "second(s) ago";
    if (diff < 3600) return Math.floor(diff/60) + "minute(s) ago";

    var result = [
        ("0" + date.getDate()).slice(-2), 
        ".",
        ("0" + date.getMonth()).slice(-2),
        ".",
        String(date.getFullYear()).slice(-2),
        " ",
        ("0" + date.getHours()).slice(-2),
        ":",
        ("0" + date.getMinutes()).slice(-2)
    ]

    result = result.reduce((sum, el) => sum + el);
    return result;
}