// Перепишите цикл через map

var arr = ["Есть", "жизнь", "на", "Марсе"];

var arrLength = [];
for (var i = 0; i < arr.length; i++) {
  arrLength[i] = arr[i].length;
}

arrLength = arr.map(el => el.length);

// Массив частичных сумм

var arr = [ 1, 2, 3, 4, 5 ];

function getSum(arr) {
    var newArr = arr.reduce((sum, el, index) => {
        if(index == 0) {
            sum[0] = el;
            return sum;
        }
        sum[index] = sum[index-1] + el;
        return sum;
    }, []);
    return newArr;
}