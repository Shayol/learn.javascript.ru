// Как в функции отличить отсутствующий аргумент от undefined?

function f(x) {
    return arguments.length > 0 ? 1 : 0;
}
  
  f(undefined); // 1
  f(); // 0

  // Напишите функцию sum(...), которая возвращает сумму всех своих аргументов:

  function sum() {
      var sum = 0;
      for(let i=0; i < arguments.length; i++) {
          sum += arguments[i];
      }
      return sum;
  }