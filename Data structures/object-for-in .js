
//Определите, пуст ли объект
function isEmpty(obj) {
    var property = 0;
    for(key in obj) {
        property++
    }
    return !property
  }




// Свойство с наибольшим значением
"use strict";

var salaries = {
  "Вася": 100,
  "Петя": 300,
  "Даша": 250
};

var sum = 0;
for (var key in salaries) {
  sum += salaries[key]
}

alert(sum)



//Умножьте численные свойства на 2
var menu = {
    width: 200,
    height: 300,
    title: "My menu"
  };

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n)
}

function multiplyNumeric(obj) {
    for(let key in obj) {
        if(isNumeric(obj[key])) obj[key] *= 2;
    }
}
  
multiplyNumeric(menu);

