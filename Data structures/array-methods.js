// Создайте функцию addClass(obj, cls), которая добавляет в список класс cls, но только если его там еще нет:

var obj = {
    className: 'open menu'
  }

function addClass(obj, newClass) {
    var arr = obj.className ? obj.className.split(' ') : [];
    var index = arr.indexOf(newClass);
    if(index == -1) {
        arr.push(newClass) 
        obj.className = arr.join(" ");
    }
}

// Напишите функцию camelize(str), которая преобразует строки вида «my-short-string» в «myShortString»

function camelize(str) {
    if(!str) return;
    var arr = str.split('-');
    var result = arr.reduce((result, word) => result + word[0].toUpperCase() + word.substring(1))
    return result;
}

// У объекта есть свойство className, которое хранит список «классов» – слов, разделенных пробелами:
// Напишите функцию removeClass(obj, cls), которая удаляет класс cls, если он есть:
// P.S. Дополнительное усложнение. Функция должна корректно обрабатывать дублирование класса в строке:

var obj = {
    className: 'open menu'
}

function removeClass(obj, cls) {
    var arr = obj.className.split(' ');
    var newArr = arr.filter(el => el != cls );
    obj.className = newArr.join(" ");
}

// Создайте функцию filterRangeInPlace(arr, a, b), которая получает массив с числами arr и удаляет из него все числа вне диапазона a..b. То есть, проверка имеет вид a ≤ arr[i] ≤ b. Функция должна менять сам массив и ничего не возвращать.

function filterRangeInPlace(arr, a, b) {
    if(a > b) {
        var i = b;
        b = a;
        a = i;
    }
    arr.forEach((el, index, array) => {
        if(el < a || el > b) {
            array.splice(index, 1)
        }
    })
}

// Как отсортировать массив чисел в обратном порядке?

var arr = [5, 2, 1, -10, 8];

arr.sort((a, b) => b - a);

// Есть массив строк arr. Создайте массив arrSorted – из тех же элементов, но отсортированный.
// Исходный массив не должен меняться.

var arr = ["HTML", "JavaScript", "CSS"];

var arrSorted = arr.slice().sort();

// Используйте функцию sort для того, чтобы «перетрясти» элементы массива в случайном порядке.

var arr = [1, 2, 3, 4, 5];

arr.sort((a, b) => 0.5 - Math.random());

// Напишите код, который отсортирует массив объектов people по полю age.

var vasya = { name: "Вася", age: 23 };
var masha = { name: "Маша", age: 18 };
var vovochka = { name: "Вовочка", age: 6 };

var people = [ vasya , masha , vovochka ];

people.sort((a, b) => a.age - b.age );

var names = people.map(el => el.name);
alert(names);

// Напишите функцию printList(list), которая выводит элементы списка по очереди, при помощи цикла.
var list = {
    value: 1,
    next: {
      value: 2,
      next: {
        value: 3,
        next: {
          value: 4,
          next: null
        }
      }
    }
  };

function printList(list) {
    while(true) {
        console.log(list.value);
        var list = list.next;
        if(!list) break;
    }
}


function printReverseList(list) {
    var arr = [];
    while(true) {
        arr.push(list.value)
        var list = list.next;
        if(!list) break;
    }
    for(var i= arr.length - 1; i >= 0; i--) {
        console.log(arr[i])
    }
}
  
// Отфильтровать анаграммы

var arr = ["воз", "киборг", "корсет", "ЗОВ", "гробик", "костер", "сектор"];

function clean(arr) {
    var result = [];
    var obj = {};
    arr.forEach(el => {
        var letters = el.toLowerCase().split().sort().join('');
        obj[letters] = el;
    })
    
    for(key in obj) {
        result.push(obj[key]);
    }


    return result;

}

// Оставить уникальные элементы массива

function unique(arr) {
    var obj = {};
    arr.forEach(el => {
        obj[el] = 1;
    })

    return Object.keys(obj);
  }
  
  var strings = ["кришна", "кришна", "харе", "харе",
    "харе", "харе", "кришна", "кришна", "8-()"
  ];