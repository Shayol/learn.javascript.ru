//Как получить последний элемент из произвольного массива?
//У нас есть массив goods. Сколько в нем элементов – не знаем, но можем прочитать из goods.length.
//Напишите код для получения последнего элемента goods.

goods = [1,2,3,4];
alert(goods[goods.length-1]);


//Как добавить элемент в конец произвольного массива?
//У нас есть массив goods. Напишите код для добавления в его конец значения «Компьютер».

goods = [1,2,3,4]
goods.push("Компьютер"); // wrong - answer is goods[goods.length] = 'Компьютер'



// Задача из 5 шагов-строк:

//     Создайте массив styles с элементами «Джаз», «Блюз».
//     Добавьте в конец значение «Рок-н-Ролл»
//     Замените предпоследнее значение с конца на «Классика». Код замены предпоследнего значения должен работать для массивов любой длины.
//     Удалите первое значение массива и выведите его alert.
//     Добавьте в начало значения «Рэп» и «Регги».


styles = ["Джаз", "Блюз"];
styles.push("Рок-н-Ролл");
styles[length-2] = "Классика";
alert(styles.shift());
styles.unshift("Рэп", "Регги");

// Напишите код для вывода alert случайного значения из массива:
// var arr = ["Яблоко", "Апельсин", "Груша", "Лимон"];
// P.S. Код для генерации случайного целого от min to max включительно:
// var rand = min + Math.floor(Math.random() * (max + 1 - min));

var arr = ["Яблоко", "Апельсин", "Груша", "Лимон"];

function randomEl(arr) {
    var min = 0;
    var max = arr.length -1;
    var rand = min + Math.floor(Math.random() * (max + 1 - min));
    return arr[rand];

}

alert(randomEl(arr));