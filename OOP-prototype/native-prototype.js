//1
function f() {
    alert( "привет" );
  }

Function.prototype.defer = function (timeout) {
    var self = this;
    setTimeout(function() {
        self.call(null);
    }, timeout);
}

f.defer(1000);

//2
function f(a, b) {
    alert( a + b );
  }

Function.prototype.defer = function(timeout) {
    var self = this;
    return function() {
        var arg = Array.from(arguments)
        setTimeout(self.bind(null, ...arg), timeout);
    }
}

f.defer(1000)(1, 2);