//1
function CoffeeMachine(power) {

  this._power = power;

  this._waterAmount = 0;

  this._WATER_HEAT_CAPACITY = 4200;

}

CoffeeMachine.prototype._getTimeToBoil = function () {
  return this._waterAmount * this._WATER_HEAT_CAPACITY * 80 / this._power;
}

CoffeeMachine.prototype.run = function () {
  setTimeout(function () {
    alert('Кофе готов!');
  }, this._getTimeToBoil());
};

CoffeeMachine.prototype.setWaterAmount = function (amount) {
  this._waterAmount = amount;
};

var coffeeMachine = new CoffeeMachine(10000);
coffeeMachine.setWaterAmount(50);
coffeeMachine.run();

//2
function Hamster() {
  this.food = [];
}

Hamster.prototype.found = function (something) {
  this.food.push(something);
};

// Создаём двух хомяков и кормим первого
var speedy = new Hamster();
var lazy = new Hamster();

speedy.found("яблоко");
speedy.found("орех");

alert(speedy.food.length); // 2
alert(lazy.food.length); // 2 (!??)