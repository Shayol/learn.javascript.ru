//1
var animal = {
    jumps: null
  };
  var rabbit = {
    jumps: true
  };
  
  rabbit.__proto__ = animal;
  
  alert( rabbit.jumps ); // ? (1) true
  
  delete rabbit.jumps;
  
  alert( rabbit.jumps ); // ? (2) null
  
  delete animal.jumps;
  
  alert( rabbit.jumps ); // ? (3) underfined

  //2
  var animal = {
    eat: function() {
      this.full = true;
    }
  };
  
  var rabbit = {
    __proto__: animal
  };
  
  rabbit.eat();

  //rabbit

//3

var head = {
    glasses: 1
  };
  
  var table = {
    pen: 3
  };
  
  var bed = {
    sheet: 1,
    pillow: 2
  };
  
  var pockets = {
    money: 2000
  };


  pockets.__proto__ = bed;
  bed.__proto__ = table;
  table.__proto__ = heade;

  