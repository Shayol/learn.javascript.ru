//1
function Rabbit() {}
Rabbit.prototype = {
  eats: true
};

var rabbit = new Rabbit();

alert( rabbit.eats );

//true

function Rabbit() {}
Rabbit.prototype = {
  eats: true
};

var rabbit = new Rabbit();

Rabbit.prototype = {};

alert( rabbit.eats );

//underfined //wrong
function Rabbit(name) {}
Rabbit.prototype = {
  eats: true
};

var rabbit = new Rabbit();

Rabbit.prototype.eats = false;

alert( rabbit.eats );

//false

function Rabbit(name) {}
Rabbit.prototype = {
  eats: true
};

var rabbit = new Rabbit();

delete rabbit.eats; // (*)

alert( rabbit.eats );

//true

function Rabbit(name) {}
Rabbit.prototype = {
  eats: true
};

var rabbit = new Rabbit();

delete Rabbit.prototype.eats; // (*)

alert( rabbit.eats );

//undefined

//2
function Menu(options) {
   options = Object.create(options);
}
//3

function Rabbit(name) {
    this.name = name;
  }
  Rabbit.prototype.sayHi = function() {
    alert( this.name );
  };
  
  var rabbit = new Rabbit("Rabbit");

  rabbit.sayHi();//Rabbit
  Rabbit.prototype.sayHi();//undefined
  Object.getPrototypeOf(rabbit).sayHi();//undefined
  rabbit.__proto__.sayHi();//not in old IE //undefined

  //4
  function notWorking () {};

  notWorking.prototype = {};

  function Working() {};


