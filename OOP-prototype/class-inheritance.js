//1
function Animal(name) {
    this.name = name;
}

Animal.prototype.walk = function () {
    alert("ходит " + this.name);
};

function Rabbit(name) {
    this.name = name;
}
Rabbit.prototype = Animal.prototype;

Rabbit.prototype.walk = function () {
    alert("прыгает! и ходит: " + this.name);
};

//Rabbit.prototype.walk overrides Animal.prototype.walk ( Rabbit.prototype = Animal.prototype)

//2
function Animal(name) {
    this.name = name;

    this.walk = function () {
        alert("ходит " + this.name);
    };

}

function Rabbit(name) {
    Animal.apply(this, arguments);
}
Rabbit.prototype = Object.create(Animal.prototype);

Rabbit.prototype.walk = function () {
    alert("прыгает " + this.name);
};

var rabbit = new Rabbit("Кроль");
rabbit.walk();

//"ходит Кроль"  method walk from Animal will be used instead of prototype walk

//3

function Clock(options) {

    this._template = options.template;

}

Clock.prototype._render = function () {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var min = date.getMinutes();
    if (min < 10) min = '0' + min;

    var sec = date.getSeconds();
    if (sec < 10) sec = '0' + sec;

    var output = this._template.replace('h', hours).replace('m', min).replace('s', sec);

    console.log(output);
}

Clock.prototype.stop = function () {
    clearInterval(this._timer);
};

Clock.prototype.start = function () {
    this._render();
    this._timer = setInterval(this._render.bind(this), 1000);
}

var clock = new Clock({
    template: 'h:m:s'
});
clock.start();

//4
function Clock(options) {

    this._template = options.template;

}

Clock.prototype._render = function () {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var min = date.getMinutes();
    if (min < 10) min = '0' + min;

    var sec = date.getSeconds();
    if (sec < 10) sec = '0' + sec;

    var output = this._template.replace('h', hours).replace('m', min).replace('s', sec);

    console.log(output);
}

Clock.prototype.stop = function () {
    clearInterval(this._timer);
};

Clock.prototype.start = function () {
    this._render();
    this._timer = setInterval(this._render.bind(this), 1000);
}

function ExtendedClock(options) {
    Clock.apply(this, arguments);

    this._presition = options.presition || 1000;
}

ExtendedClock.prototype = Object.create(Clock.prototype);
ExtendedClock.prototype.constuctor = ExtendedClock;

ExtendedClock.prototype.start = function () {
    this._render();

    this._timer = setInterval(this._render.bind(this), this._presition);
}


var clock = new ExtendedClock({
    template: 'h:m:s',
    presition: 3000
});
clock.start();

//5

function Menu(state) {
    this._state = state || this.STATE_CLOSED;
};

Menu.prototype.STATE_OPEN = 1;
Menu.prototype.STATE_CLOSED = 0;

Menu.prototype.open = function () {
    this._state = Menu.STATE_OPEN;
};

Menu.prototype.close = function () {
    this._state = Menu.STATE_CLOSED;
};

Menu.prototype._stateAsString = function () {
    switch (this._state) {
        case Menu.STATE_OPEN:
            return 'открыто';

        case Menu.STATE_CLOSED:
            return 'закрыто';
    }
};

Menu.prototype.showState = function () {
    alert(this._stateAsString());
};


function AnimatingMenu() {
    Menu.apply(this, arguments);
} // замените на ваш код для AnimatingMenu

AnimatingMenu.STATE_ANIMATING = 2;

AnimatingMenu.prototype = Object.create(Menu.prototype);

AnimatingMenu.prototype.open = function () {
    this._state = AnimatingMenu.STATE_ANIMATING;

    var self = this;
    this._timer = setTimeout(function() {
        Menu.prototype.open.apply(self);
    }, 1000);
}

AnimatingMenu.prototype.close = function () {
    if(this._timer) clearTimeout(this._timer);
    Menu.prototype.close.apply(this);
}

AnimatingMenu.prototype._stateAsString = function () {
    if (this._state == AnimatingMenu.STATE_ANIMATING) {
        return "анимация"
    } 

    return Menu.prototype._stateAsString.apply(this);
}

// использование..

var menu = new AnimatingMenu();

menu.showState(); // закрыто

menu.open();
menu.showState(); // анимация

setTimeout(function () {
    menu.showState(); // открыто

    menu.close();
    menu.showState(); // закрыто (закрытие без анимации)
}, 1000);


//6

function Animal() {}

function Rabbit() {}
Rabbit.prototype = Object.create(Animal.prototype);

var rabbit = new Rabbit();

alert( rabbit.constructor == Rabbit ); // что выведет? false - Animal
