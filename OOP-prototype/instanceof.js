//1
function A() {}

function B() {}

A.prototype = B.prototype = {};

var a = new A();

alert( a instanceof B ); // true // a.__proto__ == B.prototype
//2
function Animal() {}

function Rabbit() {}
Rabbit.prototype = Object.create(Animal.prototype);

var rabbit = new Rabbit();

alert( rabbit instanceof Rabbit ); //true
alert( rabbit instanceof Animal );//true
alert( rabbit instanceof Object );//true