// ['x'] == 'x'
Array method toString returns 'x'

// var foo = {
//     toString: function() {
//       return 'foo';
//     },
//     valueOf: function() {
//       return 2;
//     }
//   };
  
//   alert( foo );
//   alert( foo + 1 );
//   alert( foo + "3" );

'foo'
'3'
'foo3' //wrong '23'

//Почему [] == [] неверно, а [ ] == ![ ] верно?
Don't know

// new Date(0) - 0
// new Array(1)[0] + ""
// ({})[0]
// [1] + 1
// [1,2] + [3,4]
// [] + null + 1
// [[0]][0][0]
// ({} + {})

0
"underfined"
underfined
'11'
'1,23,4'
'null1'
0
'[object Object][object Object]'

//

function sum(a) {
    var num = a;
    
    function add(b) {
         num += b;
         return add;

    };
    add.toString = function() {
        return num;
    }
    return add;
}