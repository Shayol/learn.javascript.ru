function User(fullName) {
    this.fullName = fullName;

    Object.defineProperties(this, {
        firstName: {
            get: function() {
                var first = this.fullName.split(" ");
                return first[0];
            },
            set: function(value) {
                var name = this.fullName.split(" ");
                this.fullName = value + " " + name[1];
            }
        },
        lastName: {
            get: function() {
                var last = this.fullName.split(" ");
                return last[1];
            },
            set: function(value) {
                var name = this.fullName.split(" ");
                this.fullName = name[0] + " " + value;
            }
        }

    });
  }
