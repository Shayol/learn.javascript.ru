// Вызов в контексте массива
["a", "b", function];

//Проверка синтаксиса
window Object // wrong!

// usual method call - this preserved; don't know; in strict this underfined;

// Значение this в объявлении объекта

// Возврат this

Василий

// Возврат объекта с this 

Василий

// Создайте калькулятор
var calculator = {
    read: function() {
        self.a = Number(prompt("NUmber A"));
        self.b = Number(prompt("NUmber Б"));
    },
    sum: function() {
        return self.a + self.b
    },
    mul: function() {
        return self.a*self.b
    }
}

// Цепочка вызовов
var ladder = {
    step: 0,
    up: function() { 
      this.step++;
      return this;
    },
    down: function() { 
      this.step--;
      return this;
    },
    showStep: function() { 
      alert( this.step );
    }
  };

  ladder.up().up().down().up().down().showStep();