//1

function work(a) {
    alert('work function - argument ' + a);
  }

function makeLogging(f, log) {
    return function() {
        log.push(arguments[0]);
        return f.apply(this, arguments);
    }
}

var log = [];
work = makeLogging(work, log);

work(1); 
work(5); 

for (var i = 0; i < log.length; i++) {
  alert( 'Лог:' + log[i] ); 
}

//2

function work(a, b) {
    alert( a + b ); // work - произвольная функция
  }
  
  function makeLogging(f, log) { 
      return function() {
          log.push([].slice.call(arguments));
          return f.apply(this, arguments);

      } 
   }
  
  var log = [];
  work = makeLogging(work, log);
  
  work(1, 2); // 3
  work(4, 5); // 9
  
  for (var i = 0; i < log.length; i++) {
    var args = log[i]; // массив из аргументов i-го вызова
    alert( 'Лог:' + args.join() ); // "Лог:1,2", "Лог:4,5"
  }

  //3

  function f(x) {
      return Math.random() * x; // random для удобства тестирования
  }
  
  function makeCaching(f) { 
      var cached; //should be object to remember previous arguments as well
      var arg;
      return function(a) {
          if(a == arg) return cached;
          arg = a;
          cached = f.call(this, a);
          return cached;
      }
   }
  
  f = makeCaching(f);
  
  var a, b;
  
  a = f(1);
  b = f(1);
  alert( a == b ); // true (значение закешировано)
  
  b = f(2);
  alert( a == b ); // false, другой аргумент => другое значение
  