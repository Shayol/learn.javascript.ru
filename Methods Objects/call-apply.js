function sumArgs() {
    var reduce = [].reduce;
    return reduce.call(arguments, function(a,b){
        return a + b;
    });
}

function applyAll() {
    var args = [].slice.call(arguments, 1);
    return arguments[0].apply(null, args);
}