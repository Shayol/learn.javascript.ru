// Напишите функцию sum, которая работает так: sum(a)(b) = a+b

function sum(a) {
    return function(b) {
        return a+b;
    }
}

sum(5)(-1);

// Функция - строковый буфер

function makeBuffer() {
    var buffer = '';
    return function(a) {
        if(arguments.length == 0) return buffer;
        buffer += a;
    }
}

var buffer = makeBuffer();

buffer('Замыкания');
buffer(' Использовать');
buffer(' Нужно!');

// Строковый буфер с очисткой

function makeBuffer() {
    var str = '';
    function buffer(a) {
        if(arguments.length == 0) return str;
        str += a;
    }

    buffer.clear = function() {
        str = '';
    }

    return buffer;
}

// Сортировка 

function byField (field) {
    return function(a, b) {
        return a[field] > b[field] ? 1 : -1;
      }
}

// Фильтрация через функцию
function filter(arr, func) {
    var result = [];
    arr.forEach(el => {
        if(func(el)) result.push(el);
    });
    return result;
}

function inBetween(a, b) {
    return function(el) {
         return el >= a && el <= b;
    }
}

function inArray(arr) {
    return function(el) {
        return arr.indexOf(el) != -1;
    }
}

// Армия функций
function makeArmy() {
    
      var shooters = [];
    
      for (var i = 0; i < 10; i++) {
          (function(n) {
            var shooter = function() { // функция-стрелок
                alert( n ); // выводит свой номер
              };
              shooters.push(shooter);
          })(i)        
      }
    
      return shooters;
    }
    
var army = makeArmy();
army[0]();
