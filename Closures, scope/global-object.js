// Каков будет результат кода?

// if ("a" in window) {
//   var a = 1;
// }
// alert( a );

1

// Каков будет результат (перед a нет var)?

// if ("a" in window) {
//   a = 1;
// }
// alert( a );

Error

// Каков будет результат (перед a нет var, а ниже есть)?

// if ("a" in window) {
//   a = 1;
// }
// var a;

// alert( a );

1