// Что выведет say в начале кода?

// say('Вася'); // Что выведет? Не будет ли ошибки?

// var phrase = 'Привет';

// function say(name) {
//   alert( name + ", " + phrase );
// }

undefined

// Каков будет результат выполнения этого кода?

// var value = 0;

// function f() {
//   if (1) {
//     value = true;
//   } else {
//     var value = false;
//   }

//   alert( value );
// }

// f();

// Изменится ли внешняя переменная value ?

// P.S. Какими будут ответы, если из строки var value = false убрать var?

alert( value ); //true
// global variable 'value' will not change;
//without var value global scope value will change to true



// Каков будет результат выполнения этого кода? Почему?

// function test() {

//   alert( window );

//   var window = 5;

//   alert( window );
// }

// test();



undefined
5

// Каков будет результат выполнения кода? Почему?

// var a = 5

// (function() {
//   alert(a)
// })()

Error

// Если во внутренней функции есть своя переменная с именем currentCount – можно ли в ней получить currentCount из внешней функции?

// function makeCounter() {
//   var currentCount = 1;

//   return function() {
//     var currentCount;
//     // можно ли здесь вывести currentCount из внешней функции (равный 1)?
//   };
// }

No;

// Что выведут эти вызовы, если переменная currentCount находится вне makeCounter?


var currentCount = 1;

function makeCounter() {
  return function() {
    return currentCount++;
  };
}

var counter = makeCounter();
var counter2 = makeCounter();

alert( counter() ); // 1
alert( counter() ); // 2

alert( counter2() ); // 3
alert( counter2() ); // 4