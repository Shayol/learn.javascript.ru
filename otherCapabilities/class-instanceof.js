function formatDate(date) {
    var day, month, year;
    var dateObject;
    if(Array.isArray(date)) {
        dateObject = new Date(date[0], date[1], date[2]);
    }
    else if(typeof(date) == 'number') {
        dateObject = new Date(date);
    }

    else if(typeof(date) == 'string') {

        dateObject = new Date(date);
    }

    else if(date.getTime) {
        dateObject = date;
    }

    if(dateObject.getTime) {
        year = String(dateObject.getFullYear()).slice(-2);
        month = ('0' + (dateObject.getMonth() + 1)).slice(-2);
        day = ('0' + dateObject.getDate()).slice(-2);
    
        return day + '.' + month + '.' + year;
    }

    return "Wrong format!"
}