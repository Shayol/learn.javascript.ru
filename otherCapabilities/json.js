//1
var leader = {
    name: "Василий Иванович",
    age: 35
  };

var json = JSON.stringify(leader);
var newUser = JSON.parse(json);

//2
var leader = {
    name: "Василий Иванович"
  };
  
  var soldier = {
    name: "Петька"
  };
  
  // эти объекты ссылаются друг на друга!
  leader.soldier = soldier;
  soldier.leader = leader;
  
  var team = [leader, soldier];

  //Может ли это сделать прямой вызов JSON.stringify(team)? Если нет, то почему?
  //No, circular structure

  leader.id = 1;
  soldier.id = 2;

  var str = JSON.stringify(team, (key, value) => {
    if(key == "soldier") return soldier.id;
    if(key == "leader") return leader.id;

    return value;
  });